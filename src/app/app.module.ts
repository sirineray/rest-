import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { SaisidossComponent } from './saisidoss/saisidoss.component';
import {HttpClientModule} from '@angular/common/http';
import { ModifdossComponent } from './modifdoss/modifdoss.component';

import { LiquidDossComponent } from './liquid-doss/liquid-doss.component';
import { ModifLiquidComponent } from './modif-liquid/modif-liquid.component';
import { ValidDossComponent } from './valid-doss/valid-doss.component';
import { Sidebar1Component } from './sidebar1/sidebar1.component';
import { AuthComponent } from './auth/auth.component';
import { ListDossiersComponent } from './list-dossiers/list-dossiers.component';
import {MatDialogModule} from '@angular/material';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ArrerageComponent } from './arrerage/arrerage.component';
import { OppComponent } from './opp/opp.component';
import { TribunalComponent } from './tribunal/tribunal.component';
import { HuissComponent } from './huiss/huiss.component';
import { UserService } from 'services/login/user.service';
import { TestPostComponent } from './test-post/test-post.component';
const routes: Routes = [

  {path : 'auth', component: AuthComponent},
  {path : '', redirectTo: '/auth', pathMatch: 'full'},
  {path : 'saisidoss', component: SaisidossComponent}, 

  {path : 'test', component: TestPostComponent},
  
  
  {path : 'modifdoss', component: ModifdossComponent},
  {path : 'valid-doss', component: ValidDossComponent},
  {path : 'liquid-doss', component: LiquidDossComponent},
  {path : 'modif-liquid', component: ModifLiquidComponent},
  {path : 'arrerage', component: ArrerageComponent},
    {path : 'opp', component: OppComponent},
  {path : 'tribunal', component: TribunalComponent},
  {path : 'huiss', component: HuissComponent},

  // { path: '/user-profile', title: 'Gestion Dossier',  icon: 'person', class: '' },
  // {path : 'header', component: ConnexionComponent},
];


@NgModule({
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserAnimationsModule,
     FormsModule,
     MatDialogModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),

   // AgmCoreModule.forRoot({
    //  apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'})
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    SaisidossComponent,
    ModifdossComponent,

    LiquidDossComponent,
    ModifLiquidComponent,
    ValidDossComponent,
    Sidebar1Component,
    AuthComponent,
    ListDossiersComponent,
    ArrerageComponent,
    OppComponent,
    TribunalComponent,
    HuissComponent,
    TestPostComponent,


  ],
  providers: [UserService],
  entryComponents: [ListDossiersComponent],
  bootstrap: [AppComponent],
  exports: [RouterModule]

})
export class AppModule { }
