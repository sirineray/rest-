import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifLiquidComponent } from './modif-liquid.component';

describe('ModifLiquidComponent', () => {
  let component: ModifLiquidComponent;
  let fixture: ComponentFixture<ModifLiquidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifLiquidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifLiquidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
