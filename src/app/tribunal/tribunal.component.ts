import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/assure/user.service';

@Component({
  selector: 'app-tribunal',
  templateUrl: './tribunal.component.html',
  styleUrls: ['./tribunal.component.scss']
})
export class TribunalComponent implements OnInit {

  constructor(private userService: UserService) { }

  tribunals:any;
  tribunal={
    code:"",
    libelle:""
  }

  ngOnInit() {
    this.userService.getTribunal().subscribe(
      data => {
        this.tribunals=data;
     },
    )

    
  }

    onSubmit() {
        return false;
    }

    saveTribunal(code,libelle) {
     this.tribunal.code=code;
     this.tribunal.libelle=libelle;
    this.userService.addTribunal(this.tribunal).subscribe(

      data => {
         
        this.tribunals=data;
     
      },
      
      error => console.log('ERROR: ' + error));


    }

    getTribunals(){
      this.userService.getTribunal().subscribe(
        data => {
          this.tribunals=data;
       },
      )
    }

    removeTribunal(code){
      console.log(code)
      this.userService.removeTribunal(code).subscribe(
        data => {
          this.getTribunals();
       },
      )
    }


    
}
