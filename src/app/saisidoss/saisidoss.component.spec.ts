import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisidossComponent } from './saisidoss.component';

describe('SaisidossComponent', () => {
  let component: SaisidossComponent;
  let fixture: ComponentFixture<SaisidossComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisidossComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisidossComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
