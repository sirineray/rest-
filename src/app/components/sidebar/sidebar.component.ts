import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Acceuil',  icon: 'dashboard', class: '' },
    { path: '/user-profile', title: 'Gestion Dossier',  icon: 'person', class: '' },
    { path: '/typography', title: 'liquidation',  icon: 'library_books', class: '' },
    { path: '/table-list', title: 'Consultation Dossier',  icon: 'content_paste', class: '' },

    { path: '/icons', title: 'Paiement',  icon: 'bubble_chart', class: '' },
    //{ path: '/maps', title: 'Maps',  icon: 'location_on', class: '' },

   // { path: '/notifications', title: 'Notifications',  icon: 'notifications', class: '' },
    { path: '/upgrade', title: 'Gestion Parametrage',  icon: 'unarchive', class: '' }


];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
