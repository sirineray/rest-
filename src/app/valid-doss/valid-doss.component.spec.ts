import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidDossComponent } from './valid-doss.component';

describe('ValidDossComponent', () => {
  let component: ValidDossComponent;
  let fixture: ComponentFixture<ValidDossComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidDossComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidDossComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
