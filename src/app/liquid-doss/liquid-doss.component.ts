import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms'; 
import {UserService} from '../../services/assure/user.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';
import {ListDossiersComponent} from '../list-dossiers/list-dossiers.component';

@Component({
  selector: 'app-liquid-doss',
  templateUrl: './liquid-doss.component.html',
  styleUrls: ['./liquid-doss.component.scss']
})
export class LiquidDossComponent implements OnInit {

  form: any ;
  submitted=false;
  show_rente:boolean=false;
  nom: string;
  prenom: string;
  datenaiss: Date;
  Nationalite: string;
  
  
  
  heure: number ;

  date_accident: string;
  
  Type_Declaration: string;
  

  constructor(private formbuilder:FormBuilder,private userService: UserService ,   public dialog: MatDialog) { }

  ngOnInit() {

    // validate the form 
    this.form = this.formbuilder.group({
      taux_de_jug: ['', [Validators.required ] ],
      taux_de_rente: ['',[ Validators.required ]],
      salaire_de_jug: ['', [Validators.required ] ],
      salaire_retenu:['',[ Validators.required ]],
      rente_annuel:['',[ Validators.required ]],
      taux_ipp:['',[ Validators.required ]],
      decision:['',[ Validators.required ]],
      frequence:['',[ Validators.required ]],
      rente_ment:['',[ Validators.required ]],
      mentant_capital:['',[ Validators.required ]],
    });

  }


  openDialog() {
 
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;


    this.dialog.open(ListDossiersComponent, dialogConfig);
  }

  onSubmit1() {

    this.userService.getAssuredata(this.form.matr ,this.form.cle).subscribe(

        data => {
            this.nom = data.nom;
            this.heure = data.heure;
            this.prenom = data.prenom;
            this.datenaiss = data.datenaiss;
            
            this.Nationalite = data.nationalite;
            
            this.Type_Declaration = data.type_Declaration;
           
            this.date_accident = data.date_Accident;
           

        },


        error => console.log('ERROR: ' + error));


      }



  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }
  
  // set the value of taux_rente and rente_annuel
  change_taux_rente(){
    // taux_de_rente
    this.form.controls.taux_de_rente.patchValue(Number(this.form.controls.taux_de_jug.value)/2);
    // rente_annuel
    this.form.controls.rente_annuel.patchValue(Number(this.form.controls.salaire_retenu.value)*Number(this.form.controls.taux_de_rente.value));

  }
    // set the value of salaire_retenu and rente_annuel
  change_salaire_retenu(){
    // salaire_retenu
    this.form.controls.salaire_retenu.patchValue(Number(this.form.controls.salaire_de_jug.value));
    // rente_annuel
    this.form.controls.rente_annuel.patchValue(Number(this.form.controls.salaire_retenu.value)*Number(this.form.controls.taux_de_rente.value));
  }

  // change decision 
  change_decision()
  {
    if(Number(this.form.controls.taux_ipp.value)>=15)
    {
      this.form.controls.decision.patchValue('Rente');
    }else{
      this.form.controls.decision.patchValue('Capital');
    }
  }

  // rente montionnelle
  rente_ment()
  {
    const rente_ann = Number(this.form.controls.rente_annuel.value);
    
    if(this.form.controls.frequence.value=="trimestriel")
    {
      this.form.controls.rente_ment.patchValue(rente_ann/4);
    }else{
      this.form.controls.rente_ment.patchValue(rente_ann/2);
    }
  }
 
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
        return;
    }

    // test message 
    alert("hello !");
      
  }
}
