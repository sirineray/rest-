import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidDossComponent } from './liquid-doss.component';

describe('LiquidDossComponent', () => {
  let component: LiquidDossComponent;
  let fixture: ComponentFixture<LiquidDossComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquidDossComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidDossComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
