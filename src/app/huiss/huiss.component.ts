import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/assure/user.service';

@Component({
  selector: 'app-huiss',
  templateUrl: './huiss.component.html',
  styleUrls: ['./huiss.component.scss']
})
export class HuissComponent implements OnInit {

  huis:any={};
  constructor(private userService: UserService) { }

  ngOnInit() {
  }

    onSubmit() {
        return false;
    }

    saveHuiss(huiss) {
      console.log(huiss)

      this.userService.addHuiss(huiss).subscribe(

        data => {
           
          console.log(data)
        },
        
        error => console.log('ERROR: ' + error));
  }

}
