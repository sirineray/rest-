import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HuissComponent } from './huiss.component';

describe('HuissComponent', () => {
  let component: HuissComponent;
  let fixture: ComponentFixture<HuissComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HuissComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HuissComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
