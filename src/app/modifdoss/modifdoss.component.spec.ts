import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifdossComponent } from './modifdoss.component';

describe('ModifdossComponent', () => {
  let component: ModifdossComponent;
  let fixture: ComponentFixture<ModifdossComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifdossComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifdossComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
