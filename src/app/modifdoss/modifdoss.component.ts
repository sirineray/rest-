import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SaisidossComponent} from '../saisidoss/saisidoss.component';

import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material';

import {ListDossiersComponent} from '../list-dossiers/list-dossiers.component';
import {UserService} from '../../services/assure/user.service';
import {CreationDossier} from '../../services/assure/CreationDossier';

declare var $:any;

@Component({

  selector: 'app-modif-doss',
  templateUrl: './modifdoss.component.html',
  styleUrls: ['./modifdoss.component.scss']
})
export class ModifdossComponent implements OnInit {

  form: any = {};
  private  router: Router ;
  //num_dial_modif : number =0 ;
  nom: string;
  prenom: string;
  datenaiss: Date;
  Nationalite: string;
  etat: string;
  numero_cin: number;
  lieu: string;
  datelivraison: Date;
  date_jug_Object = Date;
  adresse: string;
  pays: string;
  localite: string;
  cite: string;
  code_postale: number;
  empCle: number;
  empMat: number;
  Type_Declaration: string;
  code_com_med: string;
  date_accident: string;
  date_Acc_sai: Date;

  nomhuis: string;
  prenomhuis: string;
  adressehuis: string;
  tel: number;
  rib: number;
  fax: string;
  eMail: string;
  numPat: number;
  date_juge_string: string ;
  heure: number ;
  N_jug:number;
  trub:number;
  constructor(private userService: UserService ,   public dialog: MatDialog ) {
  }
 
  ngOnInit() {


  }

  openDialog() {
 
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;


    this.dialog.open(ListDossiersComponent, dialogConfig);

     
    this.chargement();
   
  }
  navigate() {

    this.router.navigate(['user-profile']);
  }

  onSubmit() {
    return false;
  }

  save_dossier() {

  }

  

    huisData() {

    }


    chargement(){


      this.userService.getAssuredata(this.form.matr, this.form.cle).subscribe(
        data => {
            this.nom = data.nom;
            this.heure = data.heure;
            this.prenom = data.prenom;
            this.datenaiss = data.datenaiss;
            this.numero_cin = data.numeroCin;
            this.lieu = data.lieu;
            this.Nationalite = data.nationalite;
            this.datelivraison = data.datelivraison;
            this.adresse = data.adresse;
            this.pays = data.pays;
            this.localite = data.localite;
            this.cite = data.cite;
            this.code_postale = data.codePostale;
            this.etat = data.etat;
            this.empCle = data.empCle;
            this.empMat = data.empMat;
            this.Type_Declaration = data.type_Declaration;
            this.code_com_med = data.code_Com_Med;
            this.date_accident = data.date_Accident;
            this.date_Acc_sai = data.date_Saisie_Dos;
            this.getInfoJug();

            this.huiss();

        },


        error => console.log('chargement fin doss: ' + error));
        
           
    
    }
    

    getInfoJug()
    {

      console.log('chargement jug: ' );

      this.userService.getDossierdata(this.form.code_br, this.form.num_dial_modif,this.form.annee_doss).subscribe(
        data => {
          this.date_juge_string = data.dateJugement;
          this.N_jug= data.numJugement;
          this.trub = data.tri_code;
        },

        error => console.log('chargement fin: ' + error));
  


    }
    huiss() {

      this.userService.getHuissdata(this.form.huis_code, this.form.huis_cle).subscribe(
          data => {

              this.nomhuis = data.nom;
              this.prenomhuis = data.prenom;
              this.adressehuis = data.adresse;
              this.tel = data.tel;
              this.rib = data.rib;
              this.fax = data.fax;
              this.eMail = data.eMail;
              this.numPat = data.numPat;

  }),
  error => console.log('ERROR: ' + error);

  
 
    }
    enregistrer() {


      console.log('testing .z..');

      // create object CreateDossier 
      let dossier  : CreationDossier = new CreationDossier(this.form.huis_code,
            this.form.huis_cle,
           1,
           this.form.cle ,
           this.form.matr  ,
           this.date_accident ,
            this.heure,
           null,
            this.form.date_jug,
            this.form.N_jug,
            this.empMat ,
             this.empCle);

      /*let  dossier = { 
          Jugement_PK:{
               codeBr: 6, 
               numDoss : 8819515, 
               anneDoss: 8448
          },
          huis_code : this.form.huis_code,
          huis_cle : this.form.huis_cle,
          trub :  1,
          cle : this.form.cle ,
          matr : this.form.matr  ,
          date_accident :  this.date_accident ,
          heure : this.heure,
          actjug : null,
          date_juge_string : this.form.date_jug,
          N_jug :  this.form.N_jug,
          empMat : this.empMat ,
          empCle :  this.empCle,
      }
      */
      console.log('dossier: '  +  dossier);
      this.userService.getdossjug(dossier).subscribe(

          result => {  this.success(); console.log(result)},
          err => {console.log(err);}

  );




  }

      // login  with  success 
    
      success(){ 
        // afficher un message de success
        $.notify({
          icon: "ti-check-box",
          message: "Bienvenue Sur Notre Application"
        },{
            type: "success",
            timer: 500,
            placement: {
                from: "top",
                align: "right"
            }
        });
        
        // redirection  to  dash 
        this.router.navigateByUrl('/user-profile')

        
      }




          

 
}
