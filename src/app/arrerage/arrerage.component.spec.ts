import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrerageComponent } from './arrerage.component';

describe('ArrerageComponent', () => {
  let component: ArrerageComponent;
  let fixture: ComponentFixture<ArrerageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrerageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrerageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
