import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/login/user.service';


declare var $:any;

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
    form: any = {};
    submitted: false;

    private baseUrl = 'http://localhost:8091/login/';
    res: number;
    private jwt: any;
    private loginController: any;
    private users = []; 
    
    data: any;
    
    log: number;
    pwd: string;
    username: string;

    constructor(private userService: UserService, private  router: Router) {
    }

    ngOnInit() {
    }


    onSubmit() {
        console.log('login=' + this.form.login);

        console.log('password=' + this.form.password);

        this.userService.getUser(this.form.login, this.form.password) .subscribe((data) => {

            this.data = data;

            if (data != null) {
                console.log(data);
                this.log = this.data.login_pk.login;
                this.pwd = this.data.login_pk.pwd;
                this.username = this.data.username;

                console.log('log success');
                console.log(this.log);
                console.log(this.pwd);
                console.log(this.username);

                this.success();

             
            }  
        },
        error => { this.errors() ; console.log('Error', error)}
    );

    }



    // login  with  success 
    success(){ 
        // afficher un message de success
        $.notify({
          icon: "ti-check-box",
          message: "Bienvenue Sur Notre Application"
        },{
            type: "success",
            timer: 500,
            placement: {
                from: "top",
                align: "right"
            }
        });
        
        // redirection  to  dash 
        this.router.navigateByUrl('/dashboard')
      }




       // login  with  error
    errors(){ 
        // afficher un message d'erreur
        $.notify({
          icon: "ti-check-box",
          message: "Ouups! Erreur, votre matricule ou mot de passe est invalid."
        },{
            type: "danger",
            timer: 500,
            placement: {
                from: "top",
                align: "center"
            }
        }); 
      }

}

