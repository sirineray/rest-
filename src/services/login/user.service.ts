import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {config, Observable} from 'rxjs';


import {Router} from '@angular/router';






@Injectable({
    providedIn: 'root'
})
export class UserService {

    private baseUrl = 'http://localhost:8091/login/dashboard/';
    res: number;
    private jwt: any;
    private loginController: any;


    constructor(private http: HttpClient) {
    }


    getUser(id: number, pwd: string):Observable<any>{

        return  this.http.get(`${this.baseUrl}` + id + '/' + pwd);

    }


}

