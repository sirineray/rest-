export class CreationDossier {
      huis_code: number ;
       huis_cle: number;
       trub:  number;
      cle: number;
      matr: number;
      date_accident: string  ;
      heure: number;
       actjug: string ;
       date_juge_string: string;
      N_jug: number ;
      empMat: number ;
      empCle: number;


      constructor( huis_code: number ,
            huis_cle: number,
            trub:  number,
           cle: number,
           matr: number,
           date_accident: string  ,
           heure: number,
            actjug: string ,
            date_juge_string: string,
           N_jug: number ,
           empMat: number ,
           empCle: number)
           {
                 

                 this.huis_code= huis_code ;
                 this.huis_cle= huis_cle;
                 this.trub=  trub;
                 this.cle= cle;
                 this.matr= matr;
                 this.date_accident= date_accident  ;
                 this.heure= heure;
                 this.actjug= actjug ;
                 this.date_juge_string= date_juge_string;
                 this.N_jug= N_jug ;
                 this.empMat= empMat ;
                 this.empCle= empCle;
           }
}
