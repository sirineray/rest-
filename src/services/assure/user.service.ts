import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {config, Observable} from 'rxjs';


import {Router} from '@angular/router';
import {CreationDossier} from './CreationDossier';






@Injectable({
    providedIn: 'root'
})
export class UserService {

    private baseUrl = 'http://localhost:8091/connexion/SaisiDoss/';
    private UrlHuiss = 'http://localhost:8091/huiss/get/';
    private GetTribunalList = 'http://localhost:8091/tribunal/getTribunal';
    private  UrlCreationDoss = 'http://localhost:8091/Dossjug/create';
    private  Urldossiercreation = 'http://localhost:8091/Dossjug/create';
    private UrlEtatDoss = 'http://localhost:8091/etatdossier/instance/';
    private UrlgetDossier = 'http://localhost:8091/Dossjug/get/';

    res: number;
    private jwt: any;
    private loginController: any;

    public    data: any ;
   result: any;
    public date1: string;
    public date2: string;

public static num_dial  : number=0;
public anne_dial : number=0;
public matr_dial : number=0;



    constructor(   private http: HttpClient ) {
    }

    getAssuredata(mat: number , cle: number): Observable<any> {
        return this.http.get(`${this.baseUrl}` +  mat + '/' + cle );

    }

    
    getDossierdata(br: number , num: number , anne : number): Observable<any> {
        return this.http.get(`${this.UrlgetDossier}` +  br + '/' + num +'/'+anne );

    }

    getinstacemodif (bur: number , situation: number): Observable<any> {
        return this.http.get(`${this.UrlEtatDoss}` +  bur + '/' + situation );

    }


    getHuissdata(code: number , cle: number): Observable<any> {
        return this.http.get(`${this.UrlHuiss}` +  code + '/' + cle );

    }



    getdossjug(creationDossier: CreationDossier) {
        console.log('---dossier: en cours ' );
        return this.http.post('http://localhost:8091/Dossjug/create/',creationDossier
           );
    }

    addTribunal(obj) {
        console.log('---create tribunal ' );
        return this.http.post('http://localhost:8091/tribunal/addTribunal',obj
           );
    }

    
    addHuiss(obj) {
        console.log('---create tribunal ' );
        return this.http.post('http://localhost:8091/huiss//addHuissier',obj
           );
    }

     getTribunal(): Observable<any> {
        return this.http.get(`${this.GetTribunalList}`);
    }

    removeTribunal(code){
        console.log('---delete tribunal ' );
        return this.http.delete('http://localhost:8091/tribunal/remove/'+code
           );
    }
}
